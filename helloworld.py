"""
helloworld for DevSecOps purposes
"""

import datetime as dt

import pytz
from flask import Flask, make_response, render_template

app = Flask(__name__)

@app.route('/')
def hello_world():
    """root endpoint"""
    now_east = pytz.timezone("US/Eastern").normalize(dt.datetime.now(pytz.utc))
    now_east = now_east.strftime('%A, %d %B %Y %I:%M %p')
    template = render_template('home.html',
                           time=f"Current Time in Atlanta: {now_east}",
                           title="DevSecOps Hello World Flask App - Updated Root Endpoint")
    return template

@app.route('/json')
def hello_json():
    """json endpoint"""
    return '{"data":"hello_world"}', 200, {'Content-Type': 'application/json'}

@app.route('/<page_name>')
def other_page(page_name):
    """404 handler"""
    response = make_response('The page named %s does not exist.' \
                             % page_name, 404)
    return response

if __name__ == '__main__':
    app.run(host='0.0.0.0')
